import React from "react";
import {Button, ButtonGroup, Table, Form, Col, Row, Collapse} from 'react-bootstrap';
import Test3Axios from "../../apis/Test3Axios";
import { BsFillTrashFill } from "react-icons/bs";
import { BsArrowRightShort } from "react-icons/bs";
import { BsArrow90DegLeft } from "react-icons/bs";
import { BsArrow90DegRight } from "react-icons/bs";

class Festivali extends React.Component {

    constructor(props){
        super(props);

        const search = {
            mestoOdrzavanjaId: -1,
            naziv: ""
        }

        this.state = {
            festivali: [],
            pageNo: 0,
            totalPages: 1,
            search: search,
            mesta: [],
            showSearch: false
        }
    }

    componentDidMount(){
        this.getMesta();
        this.getFestivali(0, null, null);
    }

    async getMesta(){
        try{
            const res = await Test3Axios.get("/mesta");
            this.setState({
                mesta: res.data
            })
        } catch(error){
            console.log(error)
        }
    }

    async getFestivali(newPageNo, searchParamName, searchParamValue){
        const config = {
            params: {
                pageNo: newPageNo
            }
        }
        if (this.state.search.mestoOdrzavanjaId !== -1){
            config.params["mestoOdrzavanjaId"] = this.state.search.mestoOdrzavanjaId;
        }
        if (this.state.search.naziv !== ""){
            config.params["naziv"] = this.state.search.naziv;
        }


        if (searchParamName === "mestoOdrzavanjaId") {
            config.params["mestoOdrzavanjaId"] = searchParamValue;
        }
        if (searchParamName === "naziv") {
            config.params["naziv"] = searchParamValue;
        }

        console.log(config);

        try{
            const res = await Test3Axios.get("/festivali", config);
            console.log(res.data);
            this.setState({
                festivali: res.data,
                pageNo: newPageNo,
                totalPages: res.headers["total-pages"]
            })
        } catch(error){
            console.log(error)
        }
    }

    goToAdd(){
        this.props.history.push("/festivali/add")
    }

    formatDate(date){
        return date.replace("T", " ");
    }

    getTodayDateString(){
        var todayFullDate = new Date();
        var todayFullDateString = todayFullDate.toISOString();
        var todayDateString = todayFullDateString.substring(0, 10);
        return todayDateString;
    }

    searchValueInputChange(e){
        const name = e.target.name;
        const value = e.target.value;

        let search = {...this.state.search};
        search[name] = value;
        this.setState({search: search});
        this.getFestivali(0, name, value);
    }

    searchForm(){
        return(
            <Collapse in={this.state.showSearch}>
                <Form style={{marginTop:35}} id="example-collapse-text">
                    <Col>
                        <Form.Group>
                            <Form.Label>Mesto odrzavanja</Form.Label>
                            <Form.Control
                             onChange={(e) => this.searchValueInputChange(e)}
                             name="mestoOdrzavanjaId"
                             value={this.state.search.mestoOdrzavanjaId}
                             as="select">
                                 <option value={-1}></option>
                                 {this.state.mesta.map((mesto) =>{
                                     return(
                                         <option value={mesto.id} key={mesto.id}>
                                             {mesto.punoIme}
                                         </option>
                                     )
                                 })}
                            </Form.Control>
                        </Form.Group>
                    </Col>
                    <Col>
                        <Form.Group>
                            <Form.Label>Naziv festivala</Form.Label>
                            <Form.Control
                            value={this.state.search.naziv}
                            name="naziv"
                            as="input"
                            type="text"
                            onChange={(e) => this.searchValueInputChange(e)}>
                            </Form.Control>
                        </Form.Group>
                    </Col>
                </Form>
            </Collapse>
        )
    }

    searchShowHideCheckbox(){
        return(
            <>
                <Form.Group controlId="formBasicCheckbox">
                    <Form.Check onChange={() => {this.setState({showSearch: !this.state.showSearch})}} type="checkbox"
                    aria-controls="example-collapse-text" aria-expanded={this.state.showSearch} label={this.state.showSearch? "Hide Search":"Show Search"}>
                    </Form.Check>
                </Form.Group>
            </>
        )

    }

    async delete(festivalId){
        try{
            const res = await Test3Axios.delete("/festivali/" + festivalId);
            console.log(res.data)
            alert("Festival je izbrisan iz sistema.");
            this.deleteFestivalFromState(festivalId);
        } catch(error){
            console.log(error);
            alert("Desila se greska!")
        }
    }

    deleteFestivalFromState(festivalId){
        var festivali = this.state.festivali;
        festivali.forEach((festival, index) =>{
            if (festival.id === festivalId){
                festivali.splice(index, 1);
                this.setState({festivali: festivali});
            }
        })
    }

    onInputChange(e){
        const name = e.target.name;
        const value = e.target.value;

        var change = {};
        change[name] = value;
        this.setState(change);
    }

    goToAddRezervacija(festivalId){
        this.props.history.push("/rezervacije/add/" + festivalId);
    }

    kreirajVidiRacun(narudzba){
        if (narudzba.racunId == undefined){
            return(
                <Button variant="warning" onClick={() => this.kreirajRacun(narudzba.id)}>Kreiraj racun</Button>
            )
        } else {
            return (
                <Button onClick={() => this.vidiRacun(narudzba.racunId)}>Vidi racun</Button>
            )
        }
    }

    proveraDatumFestivala(festivalZavrsetak){
        var danasnjiDatum = new Date();
        var zavrsetakDatum = new Date(festivalZavrsetak);
        if (danasnjiDatum > zavrsetakDatum){
            return true;
        } else {
            return false;
        }
    }

    async updateFestival(mestoOdrzavanjaId, festival){
        var festivalDTO = {
            id: festival.id,
            naziv: festival.naziv,
            pocetak: this.formatDate(festival.pocetak),
            zavrsetak: this.formatDate(festival.zavrsetak),
            cena: festival.cena,
            brojDostupnihKarata: festival.brojDostupnihKarata,
            mestoOdrzavanjaId: mestoOdrzavanjaId
        }
        console.log(festivalDTO);

        if(festivalDTO.mestoOdrzavanjaId === -1){
            alert("Niste ispravno uneli podatke.");
            return;
        }

        try{
            const res = await Test3Axios.put("/festivali/" + festival.id, festivalDTO)
            console.log(res);
        } catch (error){
            console.log(error);
            alert("Desila se greska!")
        }

    }

    promenaMestaForma(festival){
        return(
            <Form>
                <Col>
                    <Form.Group>
                        <Form.Control
                            onChange={(e) => this.updateFestival(e.target.value, festival)}
                            name="mestoOdrzavanjaId"
                            value={festival.mestoOdrzavanjaId}
                            as="select">
                                {this.state.mesta.map((mesto) =>{
                                    return(
                                        <option value={mesto.id} key={mesto.id}>
                                            {mesto.punoIme}
                                        </option>
                                    )
                                })}
                        </Form.Control>
                    </Form.Group>
                </Col>
            </Form>
        )
    }

    prikazDugmetaRezervisi(festival){
        if (festival.brojDostupnihKarata <= 0 || this.proveraDatumFestivala(festival.zavrsetak)){
            return true;
        } else {
            return false;
        }
    }

    render(){
        return(
            <div className="container-fluid">
                <div className="jumbotron bg-secondary text-white">
                    <h2 className="display-4">Prikaz festivala</h2>
                    <hr className="my-4"></hr>
                    {window.localStorage['role'] === "ROLE_ADMIN"?
                    <Button variant="outline-light" size="sm" onClick={() => this.goToAdd()}>Dodaj festival</Button>:null}
                </div>
                {this.searchShowHideCheckbox()}
                {this.searchForm()}
                <ButtonGroup className="float-right p-2">
                    <Button variant="outline-dark" size="sm" disabled={this.state.pageNo===0} 
                    onClick={() => this.getFestivali(this.state.pageNo-1, null, null)}><BsArrow90DegLeft/></Button>
                    <Button variant="outline-dark" size="sm" disabled={this.state.pageNo===this.state.totalPages-1} 
                    onClick={() => this.getFestivali(this.state.pageNo+1, null, null)}><BsArrow90DegRight/></Button>
                </ButtonGroup>
                <Table striped hover variant="dark" style={{marginTop:5}}>
                    <thead style={{backgroundColor: 'black', color: 'white'}}>
                        <tr>
                            <th>Naziv festivala</th>
                            <th>Mesto odrzavanja</th>
                            <th>Datum pocetka festivala</th>
                            <th>Datum zavrsetka festivala</th>
                            <th>Cena karte (RSD)</th>
                            <th>Broj preostalih karata</th>
                            {window.localStorage['role']!=="ROLE_ADMIN"?
                            <th>Rezervacija</th>:null}
                            {window.localStorage['role']==="ROLE_ADMIN"?
                            <th colSpan="1">Obrisi festival</th>:null}
                        </tr>
                    </thead>
                    <tbody>
                        {this.state.festivali.map((festival) =>{
                            return(
                                <tr key={festival.id}>
                                    <td>{festival.naziv}</td>
                                    <td>{this.promenaMestaForma(festival)}</td>
                                    <td>{this.formatDate(festival.pocetak)}</td>
                                    <td>{this.formatDate(festival.zavrsetak)}</td>
                                    <td>{festival.cena}</td>
                                    <td>{festival.brojDostupnihKarata}</td>
                                    {window.localStorage['role']!=="ROLE_ADMIN"?
                                    <td><Button hidden={this.prikazDugmetaRezervisi(festival)} 
                                    onClick={()=> this.goToAddRezervacija(festival.id)}>Rezervisi</Button></td>:null}
                                    {window.localStorage['role']==="ROLE_ADMIN"?
                                    <td><Button variant="danger" onClick={() => this.delete(festival.id)}><BsFillTrashFill/></Button></td>:null}
                                </tr>
                            )
                        })}
                    </tbody>
                </Table>
            </div>
        )
    }
}

export default Festivali;