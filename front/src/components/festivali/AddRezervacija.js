import React from "react";
import { Form, Button, Row, Col, InputGroup } from "react-bootstrap";
import Test3Axios from "../../apis/Test3Axios";

class AddRezervacija extends React.Component {

    constructor(props){
        super(props);

        this.state = {
            brojKupljenihKarata: "",
            festivalId: this.props.match.params.festivalId,
            festivalNaziv: "",
            festival: ""
        }
    }

    componentDidMount(){
        this.getFestival(this.props.match.params.festivalId);
    }

    async getFestival(festivalId){
        try{
            const res = await Test3Axios.get("/festivali/" + festivalId);
            this.setState({
                festival: res.data,
                festivalNaziv: res.data.naziv
            })
        } catch(error){
            console.log(error)
        }
    }

    formatDate(date){
        return date.replace("T", " ");
    }

    formatDate(date){
        return date.replace("T", " ");
    }

    async create(){
        var rezervacijaDTO = {
            brojKupljenihKarata: this.state.brojKupljenihKarata,
            festivalId: this.state.festivalId
        };
        console.log(rezervacijaDTO);

        if(rezervacijaDTO.brojKupljenihKarata === "" || rezervacijaDTO.festivalId === -1){
            alert("Niste ispravno uneli podatke.");
            return;
        }

        if (this.state.festival.brojDostupnihKarata < rezervacijaDTO.brojKupljenihKarata){
            alert("Festival nema dovoljno dostupnih karata.");
            return;
        }

        try{
            const res = await Test3Axios.post("/rezervacije", rezervacijaDTO);
            console.log(res);
            alert("Uspesno ste rezervisali karte.")
            this.props.history.push("/festivali");
        } catch(error){
            console.log(error);
            alert("Desila se greska!")
        }
    }

    onInputChange(e){
        const name = e.target.name;
        const value = e.target.value;

        var change = {};
        change[name] = value;
        this.setState(change);
    }

    getTodayDateString(){
        var todayFullDate = new Date();
        var todayFullDateString = todayFullDate.toISOString();
        var todayDateString = todayFullDateString.substring(0, 10);
        return todayDateString;
    }

    render(){
        return(
            <div>
                <h4>Rezervisi karte</h4>
                <Form style={{marginTop:35}}>
                    <Col>
                        <Form.Group>
                            <Form.Label>Rezervisete karte za festival</Form.Label>
                            <Form.Control
                            value={this.state.festivalNaziv}
                            name="festivalNaziv"
                            as="input"
                            type="text"
                            disabled
                            onChange={(e) => this.onInputChange(e)}
                            ></Form.Control>
                        </Form.Group>
                    </Col>
                    <Col>
                        <Form.Group>
                            <Form.Label>Dostupnih karata</Form.Label>
                            <Form.Control
                            value={this.state.festival.brojDostupnihKarata}
                            name="brojDostupnihKarata"
                            as="input"
                            type="text"
                            disabled
                            onChange={(e) => this.onInputChange(e)}
                            ></Form.Control>
                        </Form.Group>
                    </Col>
                    <Col>
                        <Form.Group>
                            <Form.Label>Koliko karata rezervisete?</Form.Label>
                            <Form.Control
                            value={this.state.brojKupljenihKarata}
                            name="brojKupljenihKarata"
                            as="input"
                            type="number"
                            step="1"
                            min="0"
                            placeholder="Broj karata"
                            onChange={(e) => this.onInputChange(e)}
                            ></Form.Control>
                        </Form.Group>
                    </Col>
                    <br/>
                    <Col>
                        <Button variant="outline-dark" size="sm" onClick={() => this.create()}>Rezervisi karte</Button>
                    </Col>
                </Form>
            </div>
            
        )
    }
}

export default AddRezervacija;
