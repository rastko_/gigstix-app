import React from "react";
import { Form, Button, Row, Col, InputGroup } from "react-bootstrap";
import Test3Axios from "../../apis/Test3Axios";

class AddFestival extends React.Component {

    constructor(props){
        super(props);

        this.state = {
            naziv: "",
            pocetak: "",
            zavrsetak: "",
            cena: "",
            brojDostupnihKarata: "",
            mestoOdrzavanjaId: -1,
            mesta: [],
        }
    }

    componentDidMount(){
        this.getMesta();
    }

    async getMesta(){
        try{
            const res = await Test3Axios.get("/mesta");
            this.setState({
                mesta: res.data
            })
        } catch(error){
            console.log(error)
        }
    }

    formatDate(date){
        return date.replace("T", " ");
    }

    async create(){
        var festivalDTO = {
            naziv: this.state.naziv,
            pocetak: this.formatDate(this.state.pocetak),
            zavrsetak: this.formatDate(this.state.zavrsetak),
            cena: this.state.cena,
            brojDostupnihKarata: this.state.brojDostupnihKarata,
            mestoOdrzavanjaId: this.state.mestoOdrzavanjaId
        };
        console.log(festivalDTO);

        if(festivalDTO.naziv === "" || festivalDTO.pocetak === "" || festivalDTO.zavrsetak === "" 
        || festivalDTO.cena === "" || festivalDTO.brojDostupnihKarata == ""  || festivalDTO.mestoOdrzavanjaId === -1){
            alert("Niste ispravno uneli podatke.");
            return;
        }
        if (new Date(festivalDTO.pocetak) >= new Date(festivalDTO.zavrsetak)){
            alert("Niste ispravno uneli datume");
            return;
        }

        try{
            const res = await Test3Axios.post("/festivali", festivalDTO);
            console.log(res);
            alert("Festival je registrovan.")
            this.props.history.push("/festivali");
        } catch(error){
            console.log(error);
            alert("Desila se greska!")
        }
    }

    onInputChange(e){
        const name = e.target.name;
        const value = e.target.value;

        var change = {};
        change[name] = value;
        this.setState(change);
    }

    getTodayDateString(){
        var todayFullDate = new Date();
        var todayFullDateString = todayFullDate.toISOString();
        var todayDateString = todayFullDateString.substring(0, 10);
        return todayDateString;
    }

    render(){
        return(
            <div>
                <h4>Dodaj novi festival</h4>
                <Form style={{marginTop:35}}>
                    <Col>
                        <Form.Group>
                            <Form.Label>Naziv festivala</Form.Label>
                            <Form.Control
                            value={this.state.naziv}
                            name="naziv"
                            as="input"
                            type="text"
                            maxLength="50"
                            placeholder="Naziv festivala"
                            onChange={(e) => this.onInputChange(e)}
                            ></Form.Control>
                        </Form.Group>
                    </Col>
                    <Col>
                        <Form.Group>
                            <Form.Label>Datum pocetka festivala</Form.Label>
                            <Form.Control
                            value={this.state.pocetak}
                            name="pocetak"
                            as="input"
                            type="date"
                            min={this.getTodayDateString()}
                            onChange={(e) => this.onInputChange(e)}
                            ></Form.Control>
                        </Form.Group>
                    </Col>
                    <Col>
                        <Form.Group>
                            <Form.Label>Datum zavrsetka festivala</Form.Label>
                            <Form.Control
                            value={this.state.zavrsetak}
                            name="zavrsetak"
                            as="input"
                            type="date"
                            min={this.getTodayDateString()}
                            onChange={(e) => this.onInputChange(e)}
                            ></Form.Control>
                        </Form.Group>
                    </Col>
                    <Col>
                        <Form.Group>
                            <Form.Label>Jedinicna cena karte</Form.Label>
                            <Form.Control
                            value={this.state.cena}
                            name="cena"
                            as="input"
                            type="number"
                            step="0.01"
                            min="0"
                            placeholder="Jedinicna cena karte"
                            onChange={(e) => this.onInputChange(e)}
                            ></Form.Control>
                        </Form.Group>
                    </Col>
                    <Col>
                        <Form.Group>
                            <Form.Label>Ukupan broj dostupnih karata</Form.Label>
                            <Form.Control
                            value={this.state.brojDostupnihKarata}
                            name="brojDostupnihKarata"
                            as="input"
                            type="number"
                            step="1"
                            min="0"
                            placeholder="Ukupan broj dostupnih karata"
                            onChange={(e) => this.onInputChange(e)}
                            ></Form.Control>
                        </Form.Group>
                    </Col>
                    <Col>
                        <Form.Group>
                            <Form.Label>Mesto odrzavanja</Form.Label>
                            <Form.Control
                            value={this.state.mestoOdrzavanjaId}
                            name="mestoOdrzavanjaId"
                            as="select"
                            onChange={(e) => this.onInputChange(e)}>
                                <option value={-1}></option>
                                {this.state.mesta.map((mesto) =>{
                                    return(
                                        <option value={mesto.id} key={mesto.id}>
                                            {mesto.punoIme}
                                        </option>
                                    )
                                })}
                            </Form.Control>
                        </Form.Group>
                    </Col>
                    <br/>
                    <Col>
                        <Button variant="outline-dark" size="sm" onClick={() => this.create()}>Kreiraj festival</Button>
                    </Col>
                </Form>
            </div>
            
        )
    }
}

export default AddFestival;
