import React from "react";
import { Form, Button, Row, Col, InputGroup } from "react-bootstrap";
import Test3Axios from "../../apis/Test3Axios";

class EditLinija extends React.Component {

    constructor(props){
        super(props);

        this.state = {
            id: this.props.match.params.id,
            brojMesta: "",
            cenaKarte: "",
            destinacija: "",
            vremePolaska: "",
            prevoznikId: -1,
            linija: "",
            prevoznici: [],
        }
    }

    componentDidMount(){
        this.getPrevoznici();
        this.getLinija(this.props.match.params.id);
    }

    async getPrevoznici(){
        try{
            const res = await Test3Axios.get("/prevoznici");
            this.setState({
                prevoznici: res.data
            })
        } catch(error){
            console.log(error)
        }
    }

    async getLinija(linijaId){
        try{
            const res = await Test3Axios.get("/linije/" + linijaId);
            this.setState({
                brojMesta: res.data.brojMesta,
                cenaKarte: res.data.cenaKarte,
                destinacija: res.data.destinacija,
                vremePolaska: res.data.vremePolaska,
                prevoznikId: res.data.prevoznikId
                })
        } catch(error){
            console.log(error)
        }
    }

    formatDate(date){
        return date.replace("T", " ");
    }

    async edit(){
        var linijaDTO = {
            id: this.state.id,
            brojMesta: this.state.brojMesta,
            cenaKarte: this.state.cenaKarte,
            destinacija: this.state.destinacija,
            vremePolaska: this.formatDate(this.state.vremePolaska),
            prevoznikId: this.state.prevoznikId
        }
        console.log(linijaDTO);

        if(linijaDTO.brojMesta === "" || linijaDTO.cenaKarte === "" || linijaDTO.destinacija === "" 
        || linijaDTO.vremePolaska === "" || linijaDTO.prevoznikId === -1){
            alert("Niste ispravno uneli podatke.");
            return;
        }

        try{
            const res = await Test3Axios.put("/linije/" + this.state.id, linijaDTO)
            console.log(res);
            alert("Linija je uspesno izmenjena.");
            this.props.history.push("/linije");
        } catch (error){
            console.log(error);
            alert("Desila se greska!")
        }

    }

    onInputChange(e){
        const name = e.target.name;
        const value = e.target.value;

        var change = {};
        change[name] = value;
        this.setState(change);
    }

    getTodayDateTimeString(){
        var todayFullDateTime = new Date();
        var todayFullDateTimeString = todayFullDateTime.toISOString();
        var todayDateTimeString = todayFullDateTimeString.substring(0, 16);
        return todayDateTimeString;
    }

    render(){
        return(
            <div>
                <h4>Izmeni podatke o liniji</h4>
                <Form style={{marginTop:35}}>
                    <Col>
                        <Form.Group>
                            <Form.Label>Broj mesta</Form.Label>
                            <Form.Control
                            value={this.state.brojMesta}
                            name="brojMesta"
                            as="input"
                            type="number"
                            step="1"
                            min="0"
                            placeholder="Broj mesta"
                            onChange={(e) => this.onInputChange(e)}
                            ></Form.Control>
                        </Form.Group>
                    </Col>
                    <Col>
                        <Form.Group>
                            <Form.Label>Cena karte</Form.Label>
                            <Form.Control
                            value={this.state.cenaKarte}
                            name="cenaKarte"
                            as="input"
                            type="number"
                            step="0.01"
                            min="0"
                            placeholder="Cena karte"
                            onChange={(e) => this.onInputChange(e)}
                            ></Form.Control>
                        </Form.Group>
                    </Col>
                    <Col>
                        <Form.Group>
                            <Form.Label>Destinacija</Form.Label>
                            <Form.Control
                            value={this.state.destinacija}
                            name="destinacija"
                            as="input"
                            type="text"
                            placeholder="Destinacija"
                            onChange={(e) => this.onInputChange(e)}
                            ></Form.Control>
                        </Form.Group>
                    </Col>
                    <Col>
                        <Form.Group>
                            <Form.Label>Vreme polaska</Form.Label>
                            <Form.Control
                            value={this.state.vremePolaska}
                            name="vremePolaska"
                            as="input"
                            type="datetime-local"
                            min = {this.getTodayDateTimeString()}
                            onChange={(e) => this.onInputChange(e)}
                            ></Form.Control>
                        </Form.Group>
                    </Col>
                    <Col>
                        <Form.Group>
                            <Form.Label>Prevoznik</Form.Label>
                            <Form.Control
                            value={this.state.prevoznikId}
                            name="prevoznikId"
                            as="select"
                            onChange={(e) => this.onInputChange(e)}>
                                <option value={-1}></option>
                                {this.state.prevoznici.map((prevoznik) =>{
                                    return(
                                        <option value={prevoznik.id} key={prevoznik.id}>
                                            {prevoznik.naziv}
                                        </option>
                                    )
                                })}
                            </Form.Control>
                        </Form.Group>
                    </Col>
                    <br/>
                    <Col>
                        <Button size="sm" onClick={() => this.edit()}>Izmeni liniju</Button>
                    </Col>
                </Form>
            </div>
            
        )
    }
}

export default EditLinija;
