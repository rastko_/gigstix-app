import React from "react";
import {Button, ButtonGroup, Table, Form, Col, Row, Collapse} from 'react-bootstrap';
import Test3Axios from "../../apis/Test3Axios";
import { BsFillTrashFill } from "react-icons/bs";
import { BsArrowRightShort } from "react-icons/bs";
import { BsArrow90DegLeft } from "react-icons/bs";
import { BsArrow90DegRight } from "react-icons/bs";

class Radnici extends React.Component {

    constructor(props){
        super(props);

        const search = {
            odeljenjeId: -1,
            jmbg: ""
        }

        this.state = {
            radnici: [],
            pageNo: 0,
            totalPages: 1,
            search: search,
            odeljenja: [],
            showSearch: false,
            brojRadnikaPoOdeljenju: "",
            
            jmbg: "",
            imePrezime: "",
            email: "",
            radniStaz: "",
            odeljenjeId: -1,

            id: "",
            odabranRadnik: ""
        }
    }

    componentDidMount(){
        this.getOdeljenja();
        this.getRadnici(0, null, null);
    }

    async getOdeljenja(){
        try{
            const res = await Test3Axios.get("/odeljenja");
            this.setState({
                odeljenja: res.data
            })
        } catch(error){
            console.log(error)
        }
    }


    async getRadnici(newPageNo, searchParamName, searchParamValue){
        const config = {
            params: {
                pageNo: newPageNo
            }
        }
        if (this.state.search.odeljenjeId !== -1){
            config.params["odeljenjeId"] = this.state.search.odeljenjeId;
        }
        if (this.state.search.jmbg !== ""){
            config.params["jmbg"] = this.state.search.jmbg;
        }

        if (searchParamName === "odeljenjeId") {
            config.params["odeljenjeId"] = searchParamValue;
        }
        if (searchParamName === "jmbg") {
            config.params["jmbg"] = searchParamValue;
        }

        console.log(config);

        try{
            const res = await Test3Axios.get("/radnici", config);
            console.log(res.data);
            this.setState({
                radnici: res.data,
                pageNo: newPageNo,
                totalPages: res.headers["total-pages"]
            })
            if (res.headers["br-radnika-odeljenje"]){
                this.setState({
                    brojRadnikaPoOdeljenju: res.headers["br-radnika-odeljenje"]
                })
            } else {
                this.setState({
                    brojRadnikaPoOdeljenju: ""
                })
            }
        } catch(error){
            console.log(error)
        }
    }

    // goToAdd(){
    //     this.props.history.push("/narudzbe/add")
    // }

    formatDate(date){
        return date.replace("T", " ");
    }

    searchValueInputChange(e){
        const name = e.target.name;
        const value = e.target.value;

        let search = {...this.state.search};
        search[name] = value;
        this.setState({search: search});
        this.getRadnici(0, name, value);
    }

    searchForm(){
        return(
            <Collapse in={this.state.showSearch}>
                <Form style={{marginTop:35}} id="example-collapse-text">
                    <Col>
                        <Form.Group>
                            <Form.Label>Odeljenje</Form.Label>
                            <Form.Control
                             onChange={(e) => this.searchValueInputChange(e)}
                             name="odeljenjeId"
                             value={this.state.search.odeljenjeId}
                             as="select">
                                 <option value={-1}></option>
                                 {this.state.odeljenja.map((odeljenje) =>{
                                     return(
                                         <option value={odeljenje.id} key={odeljenje.id}>
                                             {odeljenje.ime}
                                         </option>
                                     )
                                 })}
                            </Form.Control>
                        </Form.Group>
                    </Col>
                    <Col>
                        <Form.Group>
                            <Form.Label>JMBG</Form.Label>
                            <Form.Control
                            value={this.state.search.jmbg}
                            name="jmbg"
                            as="input"
                            type="text"
                            onChange={(e) => this.searchValueInputChange(e)}>
                            </Form.Control>
                        </Form.Group>
                    </Col>
                    {/* <Col>
                        <Button onClick={() => this.getNarudzbe(0)}></Button>
                    </Col> */}
                </Form>
            </Collapse>
        )
    }

    searchShowHideCheckbox(){
        return(
            <>
                <Form.Group controlId="formBasicCheckbox">
                    <Form.Check onChange={() => {this.setState({showSearch: !this.state.showSearch})}} type="checkbox"
                    aria-controls="example-collapse-text" aria-expanded={this.state.showSearch} label={this.state.showSearch? "Hide Search":"Show Search"}>
                    </Form.Check>
                </Form.Group>
            </>
        )

    }

    // searchShowHideButton(){
    //     return(
    //         <Button variant="outline-dark" size="sm" onClick={() => {this.setState({showSearch: !this.state.showSearch})}}
    //         aria-controls="example-collapse-text" aria-expanded={this.state.showSearch}>
    //             {(this.state.showSearch)? "Hide" : "Show"} Search Form
    //         </Button>
    //     )
    // }

    async delete(radnikId){
        try{
            const res = await Test3Axios.delete("/radnici/" + radnikId);
            console.log(res.data)
            alert("Radnik je izbrisan iz sistema");
            this.deleteRadnikFromState(radnikId);
        } catch(error){
            console.log(error);
            alert("Desila se greska!")
        }
    }

    deleteRadnikFromState(radnikId){
        var radnici = this.state.radnici;
        radnici.forEach((radnik, index) =>{
            if (radnik.id === radnikId){
                radnici.splice(index, 1);
                this.setState({radnici: radnici});
            }
        })
    }

    brojRadnikaPoOdeljenju(){
        if (this.state.brojRadnikaPoOdeljenju){
            return(
                <tr>
                    <td>Odabrano odeljenje ima radnika:</td>
                    <td>{this.state.brojRadnikaPoOdeljenju}</td>
                </tr>
            )
        }
    }

    kreirajOdsustvo(radnikId){
        this.props.history.push("/odsustva/add/" + radnikId);
    }

    onInputChange(e){
        const name = e.target.name;
        const value = e.target.value;

        var change = {};
        change[name] = value;
        this.setState(change);
    }

    async create(){
        var radnikDTO = {
            jmbg: this.state.jmbg,
            imePrezime: this.state.imePrezime,
            email: this.state.email,
            radniStaz: this.state.radniStaz,
            odeljenjeId: this.state.odeljenjeId
        };
        console.log(radnikDTO);

        if(radnikDTO.jmbg === "" || radnikDTO.imePrezime === "" || radnikDTO.email === "" 
        || radnikDTO.radniStaz === "" || radnikDTO.odeljenjeId === -1){
            alert("Niste ispravno uneli podatke.");
            return;
        }

        try{
            const res = await Test3Axios.post("/radnici", radnikDTO);
            console.log(res);
            alert("Radnik je uspesno dodat u sistem.")
            window.location.reload()
        } catch(error){
            console.log(error);
            alert("Desila se greska!")
        }
    }

    async edit(){
        var radnikDTO = {
            id: this.state.id,
            jmbg: this.state.jmbg,
            imePrezime: this.state.imePrezime,
            email: this.state.email,
            radniStaz: this.state.radniStaz,
            odeljenjeId: this.state.odeljenjeId
        }
        console.log(radnikDTO);

        if(radnikDTO.jmbg === "" || radnikDTO.imePrezime === "" || radnikDTO.email === "" 
        || radnikDTO.radniStaz === "" || radnikDTO.odeljenjeId === -1){
            alert("Niste ispravno uneli podatke.");
            return;
        }

        try{
            const res = await Test3Axios.put("/radnici/" + this.state.id, radnikDTO)
            console.log(res);
            alert("Radnik je uspesno izmenjen.")
            window.location.reload()
        } catch (error){
            console.log(error);
            alert("Desila se greska!")
        }

    }

    kreirajRadnikaForma(){
        return(
            <div>
                <h4>{this.state.odabranRadnik === ""?"Dodaj novog radnika":"Izmeni podatke o radniku"}</h4>
                <Form style={{marginTop:35}}>
                    <Col>
                        <Form.Group>
                            <Form.Label>JMBG</Form.Label>
                            <Form.Control
                            value={this.state.jmbg}
                            name="jmbg"
                            as="input"
                            type="text"
                            placeholder="JMBG"
                            onChange={(e) => this.onInputChange(e)}
                            ></Form.Control>
                        </Form.Group>
                    </Col>
                    <Col>
                        <Form.Group>
                            <Form.Label>Ime i prezime</Form.Label>
                            <Form.Control
                            value={this.state.imePrezime}
                            name="imePrezime"
                            as="input"
                            type="text"
                            placeholder="Ime i prezime"
                            onChange={(e) => this.onInputChange(e)}
                            ></Form.Control>
                        </Form.Group>
                    </Col>
                    <Col>
                        <Form.Group>
                            <Form.Label>Email</Form.Label>
                            <Form.Control
                            value={this.state.email}
                            name="email"
                            as="input"
                            type="email"
                            placeholder="Email"
                            onChange={(e) => this.onInputChange(e)}
                            ></Form.Control>
                        </Form.Group>
                    </Col>
                    <Col>
                        <Form.Group>
                            <Form.Label>Godina radnog staza</Form.Label>
                            <Form.Control
                            value={this.state.radniStaz}
                            name="radniStaz"
                            as="input"
                            type="number"
                            step="1"
                            min="0"
                            placeholder="Godina radnog staza"
                            onChange={(e) => this.onInputChange(e)}
                            ></Form.Control>
                        </Form.Group>
                    </Col>
                    <Col>
                        <Form.Group>
                            <Form.Label>Odeljenje</Form.Label>
                            <Form.Control
                            value={this.state.odeljenjeId}
                            name="odeljenjeId"
                            as="select"
                            onChange={(e) => this.onInputChange(e)}>
                                <option value={-1}></option>
                                {this.state.odeljenja.map((odeljenje) =>{
                                    return(
                                        <option value={odeljenje.id} key={odeljenje.id}>
                                            {odeljenje.ime}
                                        </option>
                                    )
                                })}
                            </Form.Control>
                        </Form.Group>
                    </Col>
                    <br/>
                    <Col hidden={this.state.odabranRadnik != ""}>
                        <Button variant="outline-light" size="sm" onClick={() => this.create()}>Dodaj radnika</Button>
                    </Col>
                    <br/>
                    <Col hidden={this.state.odabranRadnik == ""}>
                        <Button variant="outline-light" size="sm" onClick={() => this.edit()}>Izmeni radnika</Button>
                    </Col>
                    <br/>
                    <Col hidden={this.state.odabranRadnik == ""}>
                        <Button variant="outline-light" size="sm" onClick={() => this.resetujFormu()}>Vrati formu za unos novog radnika</Button>
                    </Col>
                </Form>
            </div>
            
        )
    }

    editujRadnika(radnik){
        this.setState({
            jmbg: radnik.jmbg,
            imePrezime: radnik.imePrezime,
            email: radnik.email,
            radniStaz: radnik.radniStaz,
            odeljenjeId: radnik.odeljenjeId,

            id: radnik.id,
            odabranRadnik: radnik
        })
    }

    resetujFormu(){
        this.setState({
            jmbg: "",
            imePrezime: "",
            email: "",
            radniStaz: "",
            odeljenjeId: -1,

            id: "",
            odabranRadnik: ""
        })
    }


    render(){
        return(
            <div className="container">
                <div className="jumbotron bg-secondary text-white">
                    <h2 className="display-4">Radnici</h2>
                    <hr className="my-4"></hr>
                    {/* {window.localStorage['role'] === "ROLE_ADMIN"?
                    <Button variant="outline-light" size="sm" onClick={() => this.goToAdd()}>Dodaj narudzbu</Button>:null} */}
                    {this.kreirajRadnikaForma()}
                </div>
                {this.searchShowHideCheckbox()}
                {this.searchForm()}
                <ButtonGroup className="float-right p-2">
                    <Button variant="outline-dark" size="sm" disabled={this.state.pageNo===0} 
                    onClick={() => this.getRadnici(this.state.pageNo-1, null, null)}><BsArrow90DegLeft/></Button>
                    <Button variant="outline-dark" size="sm" disabled={this.state.pageNo===this.state.totalPages-1} 
                    onClick={() => this.getRadnici(this.state.pageNo+1, null, null)}><BsArrow90DegRight/></Button>
                </ButtonGroup>
                <Table striped hover variant="dark" style={{marginTop:5}}>
                    <thead style={{backgroundColor: 'black', color: 'white'}}>
                        <tr>
                            <th>JMBG</th>
                            <th>Ime i prezime</th>
                            <th>Email</th>
                            <th>Slobodni dani</th>
                            <th>Odeljenje</th>
                            <th colSpan="2">Akcije</th>
                            {/* {window.localStorage['role']==="ROLE_ADMIN"?
                            <th colSpan="2">Akcije</th>:null} */}
                        </tr>
                    </thead>
                    <tbody>
                        {this.state.radnici.map((radnik) =>{
                            return(
                                <tr key={radnik.id} onClick={() => this.editujRadnika(radnik)}>
                                    <td>{radnik.jmbg}</td>
                                    <td>{radnik.imePrezime}</td>
                                    <td>{radnik.email}</td>
                                    <td>{radnik.slobodnihDana}</td>
                                    <td>{radnik.odeljenjeIme}</td>
                                    <td><Button disabled={radnik.slobodnihDana <= 0} onClick={() => this.kreirajOdsustvo(radnik.id)}>Odsustvo</Button></td>
                                    {window.localStorage['role']==="ROLE_ADMIN"?
                                    <td><Button variant="danger" onClick={() => this.delete(radnik.id)}><BsFillTrashFill/></Button></td>:null}
                                    {/* {window.localStorage['role']==="ROLE_ADMIN"?
                                    [
                                    <td><Button disabled={zadatak.stanjeId==3} onClick={()=> this.promeniStanje(zadatak.id)}>Predji na sledece stanje</Button></td>,
                                    <td><Button onClick={()=> this.goToEdit(zadatak.id)}>Edit</Button></td>
                                    ]:null} */}
                                </tr>
                            )
                        })}
                        {this.brojRadnikaPoOdeljenju()}
                    </tbody>
                </Table>
            </div>
        )
    }
}

export default Radnici;