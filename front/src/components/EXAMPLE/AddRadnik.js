import React from "react";
import { Form, Button, Row, Col, InputGroup } from "react-bootstrap";
import Test3Axios from "../../apis/Test3Axios";

class AddRadnik extends React.Component {

    constructor(props){
        super(props);

        this.state = {
            jmbg: "",
            imePrezime: "",
            email: "",
            godinaRadnogStaza: "",
            odeljenjeId: -1,
            odeljenja: [],
        }
    }

    componentDidMount(){
        this.getOdeljenja();
    }

    async getOdeljenja(){
        try{
            const res = await Test3Axios.get("/odeljenja");
            this.setState({
                odeljenja: res.data
            })
        } catch(error){
            console.log(error)
        }
    }

    formatDate(date){
        return date.replace("T", " ");
    }

    async create(){
        var radnikDTO = {
            jmbg: this.state.jmbg,
            imePrezime: this.state.imePrezime,
            email: this.state.email,
            godinaRadnogStaza: this.state.godinaRadnogStaza,
            odeljenjeId: this.state.odeljenjeId
        };
        console.log(radnikDTO);

        if(radnikDTO.jmbg === "" || radnikDTO.imePrezime === "" || radnikDTO.email === "" 
        || radnikDTO.godinaRadnogStaza === "" || radnikDTO.odeljenjeId === -1){
            alert("Niste ispravno uneli podatke.");
            return;
        }

        try{
            const res = await Test3Axios.post("/radnici", radnikDTO);
            console.log(res);
            alert("Radnik je registrovan.")
            this.props.history.push("/radnici");
        } catch(error){
            console.log(error);
            alert("Desila se greska!")
        }
    }

    onInputChange(e){
        const name = e.target.name;
        const value = e.target.value;

        var change = {};
        change[name] = value;
        this.setState(change);
    }

    getTodayDateTimeString(){
        var todayFullDateTime = new Date();
        var todayFullDateTimeString = todayFullDateTime.toISOString();
        var todayDateTimeString = todayFullDateTimeString.substring(0, 16);
        return todayDateTimeString;
    }

    getTodayDateString(){
        var todayFullDate = new Date();
        var todayFullDateString = todayFullDate.toISOString();
        var todayDateString = todayFullDateString.substring(0, 10);
        return todayDateString;
    }

    render(){
        return(
            <div>
                <h4>Dodaj novog radnika</h4>
                <Form style={{marginTop:35}}>
                    <Col>
                        <Form.Group>
                            <Form.Label>JMBG</Form.Label>
                            <Form.Control
                            value={this.state.jmbg}
                            name="jmbg"
                            as="input"
                            type="text"
                            maxLength="13"
                            placeholder="JMBG"
                            onChange={(e) => this.onInputChange(e)}
                            ></Form.Control>
                        </Form.Group>
                    </Col>
                    <Col>
                        <Form.Group>
                            <Form.Label>Ime i prezime</Form.Label>
                            <Form.Control
                            value={this.state.imePrezime}
                            name="imePrezime"
                            as="input"
                            type="text"
                            placeholder="Ime i prezime"
                            onChange={(e) => this.onInputChange(e)}
                            ></Form.Control>
                        </Form.Group>
                    </Col>
                    <Col>
                        <Form.Group>
                            <Form.Label>Email</Form.Label>
                            <Form.Control
                            value={this.state.email}
                            name="email"
                            as="input"
                            type="email"
                            placeholder="Email"
                            onChange={(e) => this.onInputChange(e)}
                            ></Form.Control>
                        </Form.Group>
                    </Col>
                    <Col>
                        <Form.Group>
                            <Form.Label>Godina radnog staza</Form.Label>
                            <Form.Control
                            value={this.state.godinaRadnogStaza}
                            name="godinaRadnogStaza"
                            as="input"
                            type="number"
                            step="1"
                            min="0"
                            placeholder="Godina radnog staza"
                            onChange={(e) => this.onInputChange(e)}
                            ></Form.Control>
                        </Form.Group>
                    </Col>
                    <Col>
                        <Form.Group>
                            <Form.Label>Odeljenje</Form.Label>
                            <Form.Control
                            value={this.state.odeljenjeId}
                            name="odeljenjeId"
                            as="select"
                            onChange={(e) => this.onInputChange(e)}>
                                <option value={-1}></option>
                                {this.state.odeljenja.map((odeljenje) =>{
                                    return(
                                        <option value={odeljenje.id} key={odeljenje.id}>
                                            {odeljenje.ime}
                                        </option>
                                    )
                                })}
                            </Form.Control>
                        </Form.Group>
                    </Col>
                    <br/>
                    <Col>
                        <Button variant="outline-dark" size="sm" onClick={() => this.create()}>Registruj radnika</Button>
                    </Col>
                </Form>
            </div>
            
        )
    }
}

export default AddRadnik;
