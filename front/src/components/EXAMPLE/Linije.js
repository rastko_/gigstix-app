import React from "react";
import {Button, ButtonGroup, Table, Form, Col, Row, Collapse} from 'react-bootstrap';
import Test3Axios from "../../apis/Test3Axios";
import { BsFillTrashFill } from "react-icons/bs";
import { BsArrowRightShort } from "react-icons/bs";
import { BsArrow90DegLeft } from "react-icons/bs";
import { BsArrow90DegRight } from "react-icons/bs";

class Linije extends React.Component {

    constructor(props){
        super(props);

        const search = {
            prevoznikId: -1,
            destinacija: "",
            maxCena: ""
        }

        this.state = {
            linije: [],
            pageNo: 0,
            totalPages: 1,
            search: search,
            prevoznici: [],
            showSearch: false,
            brojLinijaZaPrevoznika: "",
            
            brojMesta: "",
            cenaKarte: "",
            destinacija: "",
            vremePolaska: "",
            prevoznikId: -1
        }
    }

    componentDidMount(){
        this.getPrevoznici();
        this.getLinije(0);
    }

    async getPrevoznici(){
        try{
            const res = await Test3Axios.get("/prevoznici");
            this.setState({
                prevoznici: res.data
            })
        } catch(error){
            console.log(error)
        }
    }

    async getLinije(newPageNo){
        const config = {
            params: {
                pageNo: newPageNo
            }
        }
        if (this.state.search.prevoznikId !== -1){
            config.params["prevoznikId"] = this.state.search.prevoznikId;
        }
        if (this.state.search.destinacija !== ""){
            config.params["destinacija"] = this.state.search.destinacija;
        }
        if (this.state.search.maxCena !== ""){
            config.params["maxCena"] = this.state.search.maxCena;
        }

        console.log(config);

        try{
            const res = await Test3Axios.get("/linije", config);
            console.log(res.data);
            this.setState({
                linije: res.data,
                pageNo: newPageNo,
                totalPages: res.headers["total-pages"]
            })
            if (res.headers["br-linija-sprint"]){
                this.setState({
                    brojLinijaZaPrevoznika: res.headers["br-linija-sprint"]
                })
            } else {
                this.setState({
                    brojLinijaZaPrevoznika: ""
                })
            }
        } catch(error){
            console.log(error)
        }
    }

    goToEdit(linijaId){
        this.props.history.push("/linije/edit/" + linijaId)
    }

    formatDate(date){
        return date.replace("T", " ");
    }

    searchValueInputChange(e){
        const name = e.target.name;
        const value = e.target.value;

        let search = {...this.state.search};
        search[name] = value;
        this.setState({search: search});
        // this.getRadnici(0, name, value);
    }

    searchForm(){
        return(
            <Collapse in={this.state.showSearch}>
                <Form style={{marginTop:35}} id="example-collapse-text">
                    <Col>
                        <Form.Group>
                            <Form.Label>Prevoznik</Form.Label>
                            <Form.Control
                             onChange={(e) => this.searchValueInputChange(e)}
                             name="prevoznikId"
                             value={this.state.search.prevoznikId}
                             as="select">
                                 <option value={-1}></option>
                                 {this.state.prevoznici.map((prevoznik) =>{
                                     return(
                                         <option value={prevoznik.id} key={prevoznik.id}>
                                             {prevoznik.naziv}
                                         </option>
                                     )
                                 })}
                            </Form.Control>
                        </Form.Group>
                    </Col>
                    <Col>
                        <Form.Group>
                            <Form.Label>Destinacija</Form.Label>
                            <Form.Control
                            value={this.state.search.destinacija}
                            name="destinacija"
                            as="input"
                            type="text"
                            onChange={(e) => this.searchValueInputChange(e)}>
                            </Form.Control>
                        </Form.Group>
                    </Col>
                    <Col>
                        <Button onClick={() => this.getLinije(0)}>Pretrazi</Button>
                    </Col>
                </Form>
            </Collapse>
        )
    }

    searchShowHideCheckbox(){
        return(
            <>
                <Form.Group controlId="formBasicCheckbox">
                    <Form.Check onChange={() => {this.setState({showSearch: !this.state.showSearch})}} type="checkbox"
                    aria-controls="example-collapse-text" aria-expanded={this.state.showSearch} label={this.state.showSearch? "Hide Search":"Show Search"}>
                    </Form.Check>
                </Form.Group>
            </>
        )

    }

    // searchShowHideButton(){
    //     return(
    //         <Button variant="outline-dark" size="sm" onClick={() => {this.setState({showSearch: !this.state.showSearch})}}
    //         aria-controls="example-collapse-text" aria-expanded={this.state.showSearch}>
    //             {(this.state.showSearch)? "Hide" : "Show"} Search Form
    //         </Button>
    //     )
    // }

    async delete(linijaId){
        try{
            const res = await Test3Axios.delete("/linije/" + linijaId);
            console.log(res.data)
            alert("Linija je izbrisana.");
            this.deleteLinijaFromState(linijaId);
        } catch(error){
            console.log(error);
            alert("Desila se greska!")
        }
    }

    deleteLinijaFromState(linijaId){
        var linije = this.state.linije;
        linije.forEach((linija, index) =>{
            if (linija.id === linijaId){
                linije.splice(index, 1);
                this.setState({linije: linije});
            }
        })
    }

    brojLinijaZaPrevoznika(){
        if (this.state.brojLinijaZaPrevoznika){
            return(
                <tr>
                    <td>Broj linija za odabranog prevoznika:</td>
                    <td>{this.state.brojLinijaZaPrevoznika}</td>
                </tr>
            )
        }
    }

    getTodayDateTimeString(){
        var todayFullDateTime = new Date();
        var todayFullDateTimeString = todayFullDateTime.toISOString();
        var todayDateTimeString = todayFullDateTimeString.substring(0, 16);
        return todayDateTimeString;
    }

    onInputChange(e){
        const name = e.target.name;
        const value = e.target.value;

        var change = {};
        change[name] = value;
        this.setState(change);
    }

    async create(){
        var linijaDTO = {
            brojMesta: this.state.brojMesta,
            cenaKarte: this.state.cenaKarte,
            destinacija: this.state.destinacija,
            vremePolaska: this.formatDate(this.state.vremePolaska),
            prevoznikId: this.state.prevoznikId
        };
        console.log(linijaDTO);

        if(linijaDTO.brojMesta === "" || linijaDTO.cenaKarte === "" || linijaDTO.destinacija === "" 
        || linijaDTO.vremePolaska === "" || linijaDTO.prevoznikId === -1){
            alert("Niste ispravno uneli podatke.");
            return;
        }

        try{
            const res = await Test3Axios.post("/linije", linijaDTO);
            console.log(res);
            alert("Linija je uspesno dodata.")
            window.location.reload()
        } catch(error){
            console.log(error);
            alert("Desila se greska!")
        }
    }

    async rezervisi(linijaId){
        var rezervacijaDTO = {
            linijaId: linijaId
        };
        console.log(rezervacijaDTO);

        try{
            const res = await Test3Axios.post("/rezervacije", rezervacijaDTO);
            console.log(res);
            alert("Uspesno ste rezervisali kartu.")
            window.location.reload()
        } catch(error){
            console.log(error);
            alert("Desila se greska!")
        }
    }

    proveraDostupnaMesta(linija){
        if (linija.brojMesta > 0){
            return false;
        } else {
            return true;
        }
    }

    kreirajLinijuForma(){
        return(
            <div>
                <h4>Dodaj novu liniju</h4>
                <Form style={{marginTop:35}}>
                    <Col>
                        <Form.Group>
                            <Form.Label>Broj mesta</Form.Label>
                            <Form.Control
                            value={this.state.brojMesta}
                            name="brojMesta"
                            as="input"
                            type="number"
                            step="1"
                            min="0"
                            placeholder="Broj mesta"
                            onChange={(e) => this.onInputChange(e)}
                            ></Form.Control>
                        </Form.Group>
                    </Col>
                    <Col>
                        <Form.Group>
                            <Form.Label>Cena karte</Form.Label>
                            <Form.Control
                            value={this.state.cenaKarte}
                            name="cenaKarte"
                            as="input"
                            type="number"
                            step="0.01"
                            min="0"
                            placeholder="Cena karte"
                            onChange={(e) => this.onInputChange(e)}
                            ></Form.Control>
                        </Form.Group>
                    </Col>
                    <Col>
                        <Form.Group>
                            <Form.Label>Destinacija</Form.Label>
                            <Form.Control
                            value={this.state.destinacija}
                            name="destinacija"
                            as="input"
                            type="text"
                            placeholder="Destinacija"
                            onChange={(e) => this.onInputChange(e)}
                            ></Form.Control>
                        </Form.Group>
                    </Col>
                    <Col>
                        <Form.Group>
                            <Form.Label>Vreme polaska</Form.Label>
                            <Form.Control
                            value={this.state.vremePolaska}
                            name="vremePolaska"
                            as="input"
                            type="datetime-local"
                            min = {this.getTodayDateTimeString()}
                            onChange={(e) => this.onInputChange(e)}
                            ></Form.Control>
                        </Form.Group>
                    </Col>
                    <Col>
                        <Form.Group>
                            <Form.Label>Prevoznik</Form.Label>
                            <Form.Control
                            value={this.state.prevoznikId}
                            name="prevoznikId"
                            as="select"
                            onChange={(e) => this.onInputChange(e)}>
                                <option value={-1}></option>
                                {this.state.prevoznici.map((prevoznik) =>{
                                    return(
                                        <option value={prevoznik.id} key={prevoznik.id}>
                                            {prevoznik.naziv}
                                        </option>
                                    )
                                })}
                            </Form.Control>
                        </Form.Group>
                    </Col>
                    <br/>
                    <Col>
                        <Button variant="outline-light" size="sm" onClick={() => this.create()}>Dodaj liniju</Button>
                    </Col>
                </Form>
            </div>
            
        )
    }

    render(){
        return(
            <div className="container">
                <div className="jumbotron bg-secondary text-white">
                    <h2 className="display-4">Prikaz linija</h2>
                    <hr className="my-4"></hr>
                    {/* {window.localStorage['role'] === "ROLE_ADMIN"?
                    <Button variant="outline-light" size="sm" onClick={() => this.goToAdd()}>Dodaj narudzbu</Button>:null} */}
                    {this.kreirajLinijuForma()}
                </div>
                {this.searchShowHideCheckbox()}
                {this.searchForm()}
                <ButtonGroup className="float-right p-2">
                    <Button variant="outline-dark" size="sm" disabled={this.state.pageNo===0} 
                    onClick={() => this.getLinije(this.state.pageNo-1)}><BsArrow90DegLeft/></Button>
                    <Button variant="outline-dark" size="sm" disabled={this.state.pageNo===this.state.totalPages-1} 
                    onClick={() => this.getLinije(this.state.pageNo+1)}><BsArrow90DegRight/></Button>
                </ButtonGroup>
                <Table striped hover variant="dark" style={{marginTop:5}}>
                    <thead style={{backgroundColor: 'black', color: 'white'}}>
                        <tr>
                            <th>Naziv prevoznika</th>
                            <th>Destinacija</th>
                            <th>Broj mesta</th>
                            <th>Vreme polaska</th>
                            <th>Cena karte</th>
                            {window.localStorage['role']==="ROLE_ADMIN"?
                            <th colSpan="3">Akcije</th>:null}
                        </tr>
                    </thead>
                    <tbody>
                        {this.state.linije.map((linija) =>{
                            return(
                                <tr key={linija.id}>
                                    <td>{linija.prevoznikNaziv}</td>
                                    <td>{linija.destinacija}</td>
                                    <td>{linija.brojMesta}</td>
                                    <td>{this.formatDate(linija.vremePolaska)}</td>
                                    <td>{linija.cenaKarte}</td>
                                    <td><Button disabled={this.proveraDostupnaMesta(linija)} onClick={() => this.rezervisi(linija.id)}>Rezervisi</Button></td>
                                    {window.localStorage['role']==="ROLE_ADMIN"?
                                    <td><Button variant="warning" onClick={() => this.goToEdit(linija.id)}>Izmeni</Button></td>:null}
                                    {window.localStorage['role']==="ROLE_ADMIN"?
                                    <td><Button variant="danger" onClick={() => this.delete(linija.id)}><BsFillTrashFill/></Button></td>:null}
                                    {/* {window.localStorage['role']==="ROLE_ADMIN"?
                                    [
                                    <td><Button disabled={zadatak.stanjeId==3} onClick={()=> this.promeniStanje(zadatak.id)}>Predji na sledece stanje</Button></td>,
                                    <td><Button onClick={()=> this.goToEdit(zadatak.id)}>Edit</Button></td>
                                    ]:null} */}
                                </tr>
                            )
                        })}
                        {this.brojLinijaZaPrevoznika()}
                    </tbody>
                </Table>
            </div>
        )
    }
}

export default Linije;