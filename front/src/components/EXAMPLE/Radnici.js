import React from "react";
import {Button, ButtonGroup, Table, Form, Col, Row, Collapse} from 'react-bootstrap';
import Test3Axios from "../../apis/Test3Axios";
import { BsFillTrashFill } from "react-icons/bs";
import { BsArrowRightShort } from "react-icons/bs";
import { BsArrow90DegLeft } from "react-icons/bs";
import { BsArrow90DegRight } from "react-icons/bs";

class Radnici extends React.Component {

    constructor(props){
        super(props);

        const search = {
            odeljenjeId: -1,
            jmbg: "",
            imaSlobodneDane: false
        }

        this.state = {
            radnici: [],
            pageNo: 0,
            totalPages: 1,
            search: search,
            odeljenja: [],
            showSearch: false,
            brojRadnikaPoOdeljenju: ""
        }
    }

    componentDidMount(){
        this.getOdeljenja();
        this.getRadnici(0, null, null);
    }

    async getOdeljenja(){
        try{
            const res = await Test3Axios.get("/odeljenja");
            this.setState({
                odeljenja: res.data
            })
        } catch(error){
            console.log(error)
        }
    }

    async getRadnici(newPageNo, searchParamName, searchParamValue){
        const config = {
            params: {
                pageNo: newPageNo
            }
        }
        if (this.state.search.odeljenjeId !== -1){
            config.params["odeljenjeId"] = this.state.search.odeljenjeId;
        }
        if (this.state.search.jmbg !== ""){
            config.params["jmbg"] = this.state.search.jmbg;
        }
        if (this.state.search.imaSlobodneDane !== ""){
            config.params["imaSlobodneDane"] = this.state.search.imaSlobodneDane;
        }

        if (searchParamName === "odeljenjeId") {
            config.params["odeljenjeId"] = searchParamValue;
        }
        if (searchParamName === "jmbg") {
            config.params["jmbg"] = searchParamValue;
        }
        if (searchParamName === "imaSlobodneDane") {
            config.params["imaSlobodneDane"] = !this.state.search.imaSlobodneDane;
        }

        console.log(config);

        try{
            const res = await Test3Axios.get("/radnici", config);
            console.log(res.data);
            this.setState({
                radnici: res.data,
                pageNo: newPageNo,
                totalPages: res.headers["total-pages"]
            })
            if (res.headers["br-radnika-odeljenje"]){
                this.setState({
                    brojRadnikaPoOdeljenju: res.headers["br-radnika-odeljenje"]
                })
            } else {
                this.setState({
                    brojRadnikaPoOdeljenju: ""
                })
            }
        } catch(error){
            console.log(error)
        }
    }

    goToAdd(){
        this.props.history.push("/radnici/add")
    }

    formatDate(date){
        return date.replace("T", " ");
    }

    getTodayDateString(){
        var todayFullDate = new Date();
        var todayFullDateString = todayFullDate.toISOString();
        var todayDateString = todayFullDateString.substring(0, 10);
        return todayDateString;
    }

    searchValueInputChange(e){
        const name = e.target.name;
        const value = e.target.value;

        let search = {...this.state.search};
        search[name] = value;
        this.setState({search: search});
        this.getRadnici(0, name, value);
    }

    checkboxSearchChange(){
        let search = {...this.state.search};
        search["imaSlobodneDane"] = !search["imaSlobodneDane"];
        this.setState({search: search });
        this.getRadnici(0, "imaSlobodneDane", null);
    }

    searchForm(){
        return(
            <Collapse in={this.state.showSearch}>
                <Form style={{marginTop:35}} id="example-collapse-text">
                    <Col>
                        <Form.Group>
                            <Form.Label>Odeljenje</Form.Label>
                            <Form.Control
                             onChange={(e) => this.searchValueInputChange(e)}
                             name="odeljenjeId"
                             value={this.state.search.odeljenjeId}
                             as="select">
                                 <option value={-1}></option>
                                 {this.state.odeljenja.map((odeljenje) =>{
                                     return(
                                         <option value={odeljenje.id} key={odeljenje.id}>
                                             {odeljenje.ime}
                                         </option>
                                     )
                                 })}
                            </Form.Control>
                        </Form.Group>
                    </Col>
                    <Col>
                        <Form.Group>
                            <Form.Label>JMBG</Form.Label>
                            <Form.Control
                            value={this.state.search.jmbg}
                            name="jmbg"
                            as="input"
                            type="text"
                            onChange={(e) => this.searchValueInputChange(e)}>
                            </Form.Control>
                        </Form.Group>
                    </Col>
                    <Col>
                        <Form.Group controlId="formBasicCheckbox">
                            <Form.Check onChange={() => {this.checkboxSearchChange()}} type="checkbox"
                            label="Prikazi samo radnike sa dostupnim slobodnim danima">
                            </Form.Check>
                        </Form.Group>
                    </Col>
                    {/* <Col>
                        <Button onClick={() => this.getLinije(0)}>Pretrazi</Button>
                    </Col> */}
                </Form>
            </Collapse>
        )
    }

    searchShowHideCheckbox(){
        return(
            <>
                <Form.Group controlId="formBasicCheckbox">
                    <Form.Check onChange={() => {this.setState({showSearch: !this.state.showSearch})}} type="checkbox"
                    aria-controls="example-collapse-text" aria-expanded={this.state.showSearch} label={this.state.showSearch? "Hide Search":"Show Search"}>
                    </Form.Check>
                </Form.Group>
            </>
        )

    }

    // searchShowHideButton(){
    //     return(
    //         <Button variant="outline-dark" size="sm" onClick={() => {this.setState({showSearch: !this.state.showSearch})}}
    //         aria-controls="example-collapse-text" aria-expanded={this.state.showSearch}>
    //             {(this.state.showSearch)? "Hide" : "Show"} Search Form
    //         </Button>
    //     )
    // }

    async delete(radnikId){
        try{
            const res = await Test3Axios.delete("/radnici/" + radnikId);
            console.log(res.data)
            alert("Radnik je izbrisan iz sistema.");
            this.deleteRadnikFromState(radnikId);
        } catch(error){
            console.log(error);
            alert("Desila se greska!")
        }
    }

    deleteRadnikFromState(radnikId){
        var radnici = this.state.radnici;
        radnici.forEach((radnik, index) =>{
            if (radnik.id === radnikId){
                radnici.splice(index, 1);
                this.setState({radnici: radnici});
            }
        })
    }

    brojRadnikaPoOdeljenju(){
        if (this.state.brojRadnikaPoOdeljenju != ""){
            return(
                <tr>
                    <td colSpan="3">Ukupan broj radnika za odabrano odeljenje:</td>
                    <td>{this.state.brojRadnikaPoOdeljenju}</td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                </tr>
            )
        }
    }

    getTodayDateTimeString(){
        var todayFullDateTime = new Date();
        var todayFullDateTimeString = todayFullDateTime.toISOString();
        var todayDateTimeString = todayFullDateTimeString.substring(0, 16);
        return todayDateTimeString;
    }

    onInputChange(e){
        const name = e.target.name;
        const value = e.target.value;

        var change = {};
        change[name] = value;
        this.setState(change);
    }

    goToAddOdsustvo(radnikId){
        this.props.history.push("/odsustva/add/" + radnikId);
    }

    kreirajVidiRacun(narudzba){
        if (narudzba.racunId == undefined){
            return(
                <Button variant="warning" onClick={() => this.kreirajRacun(narudzba.id)}>Kreiraj racun</Button>
            )
        } else {
            return (
                <Button onClick={() => this.vidiRacun(narudzba.racunId)}>Vidi racun</Button>
            )
        }
    }

    render(){
        return(
            <div className="container">
                <div className="jumbotron bg-secondary text-white">
                    <h2 className="display-4">Prikaz radnika</h2>
                    <hr className="my-4"></hr>
                    {window.localStorage['role'] === "ROLE_ADMIN"?
                    <Button variant="outline-light" size="sm" onClick={() => this.goToAdd()}>Dodaj radnika</Button>:null}
                    {/* {this.kreirajLinijuForma()} */}
                </div>
                {this.searchShowHideCheckbox()}
                {this.searchForm()}
                <ButtonGroup className="float-right p-2">
                    <Button variant="outline-dark" size="sm" disabled={this.state.pageNo===0} 
                    onClick={() => this.getRadnici(this.state.pageNo-1, null, null)}><BsArrow90DegLeft/></Button>
                    <Button variant="outline-dark" size="sm" disabled={this.state.pageNo===this.state.totalPages-1} 
                    onClick={() => this.getRadnici(this.state.pageNo+1, null, null)}><BsArrow90DegRight/></Button>
                </ButtonGroup>
                <Table striped hover variant="dark" style={{marginTop:5}}>
                    <thead style={{backgroundColor: 'black', color: 'white'}}>
                        <tr>
                            <th>JMBG</th>
                            <th>Ime i prezime</th>
                            <th>Email</th>
                            <th>Slobodni dani</th>
                            <th>Odeljenje</th>
                            <th colSpan="2">Akcije</th>
                            {/* {window.localStorage['role']==="ROLE_ADMIN"?
                            <th colSpan="2">Akcije</th>:null} */}
                        </tr>
                    </thead>
                    <tbody>
                        {this.state.radnici.map((radnik) =>{
                            return(
                                <tr key={radnik.id}>
                                    <td>{radnik.jmbg}</td>
                                    <td>{radnik.imePrezime}</td>
                                    <td>{radnik.email}</td>
                                    <td>{radnik.slobodniDani}</td>
                                    <td>{radnik.odeljenjeIme}</td>
                                    <td><Button disabled={radnik.slobodniDani <= 0 || window.localStorage['role']==="ROLE_ADMIN"} 
                                    onClick={()=> this.goToAddOdsustvo(radnik.id)}>Odsustvo</Button></td>
                                    {/* {window.localStorage['role']==="ROLE_ADMIN"?
                                    <td><Button variant="warning" onClick={() => this.goToEdit(radnik.id)}>Izmeni</Button></td>:null} */}
                                    {window.localStorage['role']==="ROLE_ADMIN"?
                                    <td><Button variant="danger" onClick={() => this.delete(radnik.id)}><BsFillTrashFill/></Button></td>:null}
                                    {/* {window.localStorage['role']==="ROLE_ADMIN"?
                                    [
                                    <td><Button disabled={zadatak.stanjeId==3} onClick={()=> this.promeniStanje(zadatak.id)}>Predji na sledece stanje</Button></td>,
                                    <td><Button onClick={()=> this.goToEdit(zadatak.id)}>Edit</Button></td>
                                    ]:null} */}
                                </tr>
                            )
                        })}
                        {this.brojRadnikaPoOdeljenju()}
                    </tbody>
                </Table>
            </div>
        )
    }
}

export default Radnici;