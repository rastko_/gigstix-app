import React from "react";
import ReactDOM from "react-dom";
import { Route, Link, HashRouter as Router, Switch, Redirect } from "react-router-dom";
import { Navbar, Nav, Container, Button } from "react-bootstrap";
import Home from "./components/Home";
import Login from './components/authorization/Login';
import NotFound from "./components/NotFound";
import {logout} from './services/auth';
import Festivali from "./components/festivali/Festivali";
import AddFestival from "./components/festivali/AddFestival";
import AddRezervacija from "./components/festivali/AddRezervacija";

class App extends React.Component {
  render() {

    const jwt = window.localStorage['jwt'];

    if(jwt){return (
      <div>
        <Router>
          <Navbar expand bg="dark" variant="dark">
            <Navbar.Brand as={Link} to="/">
                Test3
            </Navbar.Brand>
            <Nav>
              <Nav.Link as={Link} to="/festivali">
                Festivali
              </Nav.Link>
                <Button onClick={()=>logout()}>Logout</Button>
            </Nav>
          </Navbar>
          <Container style={{paddingTop:"25px"}}>
            <Switch>
              <Route exact path="/" component={Home} />
              <Route exact path="/login"  render={()=><Redirect to="/festivali"/>}/>
              <Route exact path="/festivali" component={Festivali} />
              <Route exact path="/festivali/add" component={AddFestival} />
              <Route exact path="/rezervacije/add/:festivalId" component={AddRezervacija} />
              <Route component={NotFound} />
            </Switch>
          </Container>
        </Router>
      </div>
    )}else{
      return( 
        <Container>
          <Router>
            <Switch>
              <Route exact path="/login" component={Login}/>
              <Route render={()=> <Redirect to = "/login"/>}/>
            </Switch>
          </Router>
        </Container>
    
    )}
  }
}

ReactDOM.render(<App />, document.querySelector("#root"));
