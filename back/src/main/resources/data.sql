INSERT INTO korisnik (id, e_mail, korisnicko_ime, lozinka, ime, prezime, uloga)
              VALUES (1,'miroslav@maildrop.cc','miroslav','$2y$12$NH2KM2BJaBl.ik90Z1YqAOjoPgSd0ns/bF.7WedMxZ54OhWQNNnh6','Miroslav','Simic','ADMIN');
INSERT INTO korisnik (id, e_mail, korisnicko_ime, lozinka, ime, prezime, uloga)
              VALUES (2,'tamara@maildrop.cc','tamara','$2y$12$DRhCpltZygkA7EZ2WeWIbewWBjLE0KYiUO.tHDUaJNMpsHxXEw9Ky','Tamara','Milosavljevic','KORISNIK');
INSERT INTO korisnik (id, e_mail, korisnicko_ime, lozinka, ime, prezime, uloga)
              VALUES (3,'petar@maildrop.cc','petar','$2y$12$i6/mU4w0HhG8RQRXHjNCa.tG2OwGSVXb0GYUnf8MZUdeadE4voHbC','Petar','Jovic','KORISNIK');
              
        
INSERT INTO mesto_odrzavanja (id, drzava, grad)
	VALUES (1, "AAA", "AAA Grad");
INSERT INTO mesto_odrzavanja (id, drzava, grad)
	VALUES (2, "BBB", "BBB Grad");
INSERT INTO mesto_odrzavanja (id, drzava, grad)
	VALUES (3, "CCC", "CCC Grad");
	
INSERT INTO festival (id, broj_dostupnih_karata, cena, naziv, pocetak, zavrsetak, mesto_odrzavanja_id)
	VALUES (1, 10, 100.5, "Festival 1", "2021-01-01", "2021-02-02", 1);
INSERT INTO festival (id, broj_dostupnih_karata, cena, naziv, pocetak, zavrsetak, mesto_odrzavanja_id)
	VALUES (2, 15, 110.5, "Festival 2", "2021-06-06", "2021-07-07", 2);
INSERT INTO festival (id, broj_dostupnih_karata, cena, naziv, pocetak, zavrsetak, mesto_odrzavanja_id)
	VALUES (3, 0, 120.5, "Festival 3", "2021-07-07", "2021-08-08", 1);
INSERT INTO festival (id, broj_dostupnih_karata, cena, naziv, pocetak, zavrsetak, mesto_odrzavanja_id)
	VALUES (4, 20, 100.5, "Festival 4", "2021-08-08", "2021-09-09", 2);
INSERT INTO festival (id, broj_dostupnih_karata, cena, naziv, pocetak, zavrsetak, mesto_odrzavanja_id)
	VALUES (5, 10, 106.5, "Festival 5", "2021-09-09", "2021-10-10", 3);
	
INSERT INTO rezervacija (id, broj_kupljenih_karata, ukupna_cena, festival_id)
	VALUES (1, 3, 300, 1);
INSERT INTO rezervacija (id, broj_kupljenih_karata, ukupna_cena, festival_id)
	VALUES (2, 2, 200, 2);
INSERT INTO rezervacija (id, broj_kupljenih_karata, ukupna_cena, festival_id)
	VALUES (3, 4, 400, 3);
