package com.modul3.test.jwd48rnTMP.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import com.modul3.test.jwd48rnTMP.model.Festival;
import com.modul3.test.jwd48rnTMP.repository.FestivalRepository;
import com.modul3.test.jwd48rnTMP.service.FestivalService;

@Service
public class JpaFestivalService implements FestivalService {

	@Autowired
	private FestivalRepository festivalRepository;
	
	@Override
	public Festival findOne(Long id) {
		// TODO Auto-generated method stub
		return festivalRepository.findOneById(id);
	}

	@Override
	public List<Festival> findAll() {
		// TODO Auto-generated method stub
		return festivalRepository.findAll();
	}

	@Override
	public List<Festival> find(List<Long> ids) {
		// TODO Auto-generated method stub
		return festivalRepository.findByIdIn(ids);
	}

	@Override
	public Page<Festival> findAll(int pageNo) {
		// TODO Auto-generated method stub
		return festivalRepository.findAll(PageRequest.of(pageNo, 4));
	}

	@Override
	public Festival save(Festival festival) {
		// TODO Auto-generated method stub
		return festivalRepository.save(festival);
	}

	@Override
	public Festival update(Festival festival) {
		// TODO Auto-generated method stub
		return festivalRepository.save(festival);
	}

	@Override
	public Festival delete(Long id) {
		Festival festival = findOne(id);
		if (festival != null) {
			festivalRepository.delete(festival);
			return festival;
		}
		return null;
	}

	@Override
	public Page<Festival> find(Long mestoOdrzavanjaId, String naziv, int pageNo) {
		// TODO Auto-generated method stub
		return festivalRepository.search(mestoOdrzavanjaId, naziv, PageRequest.of(pageNo, 4));
	}

}
