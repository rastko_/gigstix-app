package com.modul3.test.jwd48rnTMP.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.modul3.test.jwd48rnTMP.model.Rezervacija;

@Repository
public interface RezervacijaRepository extends JpaRepository<Rezervacija, Long> {

	Rezervacija findOneById(Long id);
}
