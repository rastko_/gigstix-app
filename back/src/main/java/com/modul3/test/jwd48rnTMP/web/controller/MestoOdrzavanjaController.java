package com.modul3.test.jwd48rnTMP.web.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.modul3.test.jwd48rnTMP.model.MestoOdrzavanja;
import com.modul3.test.jwd48rnTMP.service.MestoOdrzavanjaService;
import com.modul3.test.jwd48rnTMP.support.MestoOdrzavanjaToMestoOdrzavanjaDto;
import com.modul3.test.jwd48rnTMP.web.dto.MestoOdrzavanjaDTO;

@RestController
@RequestMapping(value = "/api/mesta", produces = MediaType.APPLICATION_JSON_VALUE)
@Validated
public class MestoOdrzavanjaController {

	@Autowired
	private MestoOdrzavanjaService mestoOdrzavanjaService;
	
	@Autowired
	private MestoOdrzavanjaToMestoOdrzavanjaDto toMestoOdrzavanjaDto;
	
	@PreAuthorize("hasAnyRole('KORISNIK', 'ADMIN')")
	@GetMapping("/{id}")
	public ResponseEntity<MestoOdrzavanjaDTO> getOne(@PathVariable Long id){
		MestoOdrzavanja mestoOdrzavanja = mestoOdrzavanjaService.findOne(id);
		if (mestoOdrzavanja != null) {
			return new ResponseEntity<>(toMestoOdrzavanjaDto.convert(mestoOdrzavanja), HttpStatus.OK);
		} else {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
	}
	
	@PreAuthorize("hasAnyRole('KORISNIK', 'ADMIN')")
	@GetMapping
	public ResponseEntity<List<MestoOdrzavanjaDTO>> getAll(){
		List<MestoOdrzavanja> mestaOdrzavanja = mestoOdrzavanjaService.findAll();
		return new ResponseEntity<>(toMestoOdrzavanjaDto.convert(mestaOdrzavanja), HttpStatus.OK);
	}
}
