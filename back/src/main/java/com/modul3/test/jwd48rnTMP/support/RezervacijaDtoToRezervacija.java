package com.modul3.test.jwd48rnTMP.support;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import com.modul3.test.jwd48rnTMP.model.Rezervacija;
import com.modul3.test.jwd48rnTMP.service.FestivalService;
import com.modul3.test.jwd48rnTMP.service.RezervacijaService;
import com.modul3.test.jwd48rnTMP.web.dto.RezervacijaDTO;

@Component
public class RezervacijaDtoToRezervacija implements Converter<RezervacijaDTO, Rezervacija> {

	@Autowired
	private RezervacijaService rezervacijaService;
	
	@Autowired
	private FestivalService festivalService;
	
	@Override
	public Rezervacija convert(RezervacijaDTO dto) {
		Rezervacija rezervacija;
		if (dto.getId() != null) {
			rezervacija = rezervacijaService.findOne(dto.getId());
		} else {
			rezervacija = new Rezervacija();
		}
		if (rezervacija != null) {
			rezervacija.setBrojKupljenihKarata(dto.getBrojKupljenihKarata());
			if (dto.getFestivalId() != null) {
				rezervacija.setFestival(festivalService.findOne(dto.getFestivalId()));
				rezervacija.setUkupnaCena(festivalService.findOne(dto.getFestivalId()).getCena() * dto.getBrojKupljenihKarata());
			}
			
		}
		
		return rezervacija;
	}

}
