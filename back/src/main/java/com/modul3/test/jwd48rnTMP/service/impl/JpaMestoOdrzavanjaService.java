package com.modul3.test.jwd48rnTMP.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.modul3.test.jwd48rnTMP.model.MestoOdrzavanja;
import com.modul3.test.jwd48rnTMP.repository.MestoOdrzavanjaRepository;
import com.modul3.test.jwd48rnTMP.service.MestoOdrzavanjaService;

@Service
public class JpaMestoOdrzavanjaService implements MestoOdrzavanjaService {

	@Autowired
	private MestoOdrzavanjaRepository mestoOdrzavanjaRepository;
	
	@Override
	public MestoOdrzavanja findOne(Long id) {
		// TODO Auto-generated method stub
		return mestoOdrzavanjaRepository.findOneById(id);
	}

	@Override
	public List<MestoOdrzavanja> findAll() {
		// TODO Auto-generated method stub
		return mestoOdrzavanjaRepository.findAll();
	}

	@Override
	public MestoOdrzavanja save(MestoOdrzavanja mestoOdrzavanja) {
		// TODO Auto-generated method stub
		return mestoOdrzavanjaRepository.save(mestoOdrzavanja);
	}

}
