package com.modul3.test.jwd48rnTMP.model;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

@Entity
public class Festival {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	@Column(nullable = false)
	private String naziv;
	
	@Column(nullable = false)
	private LocalDate pocetak;
	
	@Column(nullable = false)
	private LocalDate zavrsetak;
	
	@Column(nullable = false)
	private Double cena;
	
	@Column(nullable = false)
	private Integer brojDostupnihKarata;
	
	@ManyToOne
	private MestoOdrzavanja mestoOdrzavanja;
	
	@OneToMany(mappedBy = "festival", fetch = FetchType.EAGER, cascade = CascadeType.ALL)
	private List<Rezervacija> rezervacije = new ArrayList<>();

	public Festival() {
		super();
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Festival other = (Festival) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNaziv() {
		return naziv;
	}

	public void setNaziv(String naziv) {
		this.naziv = naziv;
	}

	public LocalDate getPocetak() {
		return pocetak;
	}

	public void setPocetak(LocalDate pocetak) {
		this.pocetak = pocetak;
	}

	public LocalDate getZavrsetak() {
		return zavrsetak;
	}

	public void setZavrsetak(LocalDate zavrsetak) {
		this.zavrsetak = zavrsetak;
	}

	public Double getCena() {
		return cena;
	}

	public void setCena(Double cena) {
		this.cena = cena;
	}

	public Integer getBrojDostupnihKarata() {
		return brojDostupnihKarata;
	}

	public void setBrojDostupnihKarata(Integer brojDostupnihKarata) {
		this.brojDostupnihKarata = brojDostupnihKarata;
	}

	public MestoOdrzavanja getMestoOdrzavanja() {
		return mestoOdrzavanja;
	}

	public void setMestoOdrzavanja(MestoOdrzavanja mestoOdrzavanja) {
		this.mestoOdrzavanja = mestoOdrzavanja;
	}

	public List<Rezervacija> getRezervacije() {
		return rezervacije;
	}

	public void setRezervacije(List<Rezervacija> rezervacije) {
		this.rezervacije = rezervacije;
	}

	@Override
	public String toString() {
		return "Festival [id=" + id + ", naziv=" + naziv + ", pocetak=" + pocetak + ", zavrsetak=" + zavrsetak
				+ ", cena=" + cena + ", brojDostupnihKarata=" + brojDostupnihKarata + "]";
	}
	
	
	
	
}
