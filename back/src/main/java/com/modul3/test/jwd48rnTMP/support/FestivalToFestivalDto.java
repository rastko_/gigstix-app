package com.modul3.test.jwd48rnTMP.support;

import java.util.ArrayList;
import java.util.List;

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import com.modul3.test.jwd48rnTMP.model.Festival;
import com.modul3.test.jwd48rnTMP.web.dto.FestivalDTO;

@Component
public class FestivalToFestivalDto implements Converter<Festival, FestivalDTO> {

	@Override
	public FestivalDTO convert(Festival festival) {
		FestivalDTO dto = new FestivalDTO();
		dto.setId(festival.getId());
		dto.setBrojDostupnihKarata(festival.getBrojDostupnihKarata());
		dto.setCena(festival.getCena());
		if (festival.getMestoOdrzavanja() != null) {
			dto.setMestoOdrzavanjaId(festival.getMestoOdrzavanja().getId());
			String mestoOdrzavanjaNaziv = festival.getMestoOdrzavanja().getGrad() + ", " + festival.getMestoOdrzavanja().getDrzava();
			dto.setMestoOdrzavanjaNaziv(mestoOdrzavanjaNaziv);
		}
		
		dto.setNaziv(festival.getNaziv());
		dto.setPocetak(festival.getPocetak().toString());
		dto.setZavrsetak(festival.getZavrsetak().toString());
		
		return dto;
	}
	public List<FestivalDTO> convert (List<Festival> festivali){
		List<FestivalDTO> dtos = new ArrayList<>();
		for (Festival festival : festivali) {
			dtos.add(convert(festival));
		}
		return dtos;
	}
}
