package com.modul3.test.jwd48rnTMP.service;

import java.util.List;

import com.modul3.test.jwd48rnTMP.model.MestoOdrzavanja;

public interface MestoOdrzavanjaService {

	MestoOdrzavanja findOne(Long id);
	
	List<MestoOdrzavanja> findAll();
	
	MestoOdrzavanja save(MestoOdrzavanja mestoOdrzavanja);	
}
