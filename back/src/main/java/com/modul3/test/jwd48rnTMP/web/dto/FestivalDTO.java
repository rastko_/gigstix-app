package com.modul3.test.jwd48rnTMP.web.dto;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Positive;
import javax.validation.constraints.Size;

import com.sun.istack.NotNull;

public class FestivalDTO {

	@Positive (message = "Id mora da bude pozitivan broj.")
	private Long id;
	
	@NotBlank (message = "Naziv - nije zadato.")
	@NotNull
	@Size(max = 50)
	private String naziv;
	
	private String pocetak;
	
	private String zavrsetak;
	
	@Positive (message = "Cena mora da bude pozitivan broj.")
	private Double cena;
	
	private Integer brojDostupnihKarata;
	
	private Long mestoOdrzavanjaId;
	
	private String mestoOdrzavanjaNaziv;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNaziv() {
		return naziv;
	}

	public void setNaziv(String naziv) {
		this.naziv = naziv;
	}

	public String getPocetak() {
		return pocetak;
	}

	public void setPocetak(String pocetak) {
		this.pocetak = pocetak;
	}

	public String getZavrsetak() {
		return zavrsetak;
	}

	public void setZavrsetak(String zavrsetak) {
		this.zavrsetak = zavrsetak;
	}

	public Double getCena() {
		return cena;
	}

	public void setCena(Double cena) {
		this.cena = cena;
	}

	public Integer getBrojDostupnihKarata() {
		return brojDostupnihKarata;
	}

	public void setBrojDostupnihKarata(Integer brojDostupnihKarata) {
		this.brojDostupnihKarata = brojDostupnihKarata;
	}

	public Long getMestoOdrzavanjaId() {
		return mestoOdrzavanjaId;
	}

	public void setMestoOdrzavanjaId(Long mestoOdrzavanjaId) {
		this.mestoOdrzavanjaId = mestoOdrzavanjaId;
	}

	public String getMestoOdrzavanjaNaziv() {
		return mestoOdrzavanjaNaziv;
	}

	public void setMestoOdrzavanjaNaziv(String mestoOdrzavanjaNaziv) {
		this.mestoOdrzavanjaNaziv = mestoOdrzavanjaNaziv;
	}
	
	
}
