package com.modul3.test.jwd48rnTMP.web.dto;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Positive;
import javax.validation.constraints.Size;

import com.sun.istack.NotNull;

public class RezervacijaDTO {

	@Positive (message = "Id mora da bude pozitivan broj.")
	private Long id;
	
	private Integer brojKupljenihKarata;
	
	private Double ukupnaCena;
	
	private Long festivalId;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Integer getBrojKupljenihKarata() {
		return brojKupljenihKarata;
	}

	public void setBrojKupljenihKarata(Integer brojKupljenihKarata) {
		this.brojKupljenihKarata = brojKupljenihKarata;
	}

	public Double getUkupnaCena() {
		return ukupnaCena;
	}

	public void setUkupnaCena(Double ukupnaCena) {
		this.ukupnaCena = ukupnaCena;
	}

	public Long getFestivalId() {
		return festivalId;
	}

	public void setFestivalId(Long festivalId) {
		this.festivalId = festivalId;
	}
	
	
}
