package com.modul3.test.jwd48rnTMP.support;

import java.util.ArrayList;
import java.util.List;

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import com.modul3.test.jwd48rnTMP.model.Rezervacija;
import com.modul3.test.jwd48rnTMP.web.dto.RezervacijaDTO;

@Component
public class RezervacijaToRezervacijaDto implements Converter<Rezervacija, RezervacijaDTO> {

	@Override
	public RezervacijaDTO convert(Rezervacija rezervacija) {
		RezervacijaDTO dto = new RezervacijaDTO();
		dto.setId(rezervacija.getId());
		dto.setBrojKupljenihKarata(rezervacija.getBrojKupljenihKarata());
		dto.setFestivalId(rezervacija.getFestival().getId());
		dto.setUkupnaCena(rezervacija.getUkupnaCena());
		return dto;
	}
	public List<RezervacijaDTO> convert (List<Rezervacija> rezervacije){
		List<RezervacijaDTO> dtos = new ArrayList<>();
		for (Rezervacija rezervacija : rezervacije) {
			dtos.add(convert(rezervacija));
		}
		return dtos;
	}
}
