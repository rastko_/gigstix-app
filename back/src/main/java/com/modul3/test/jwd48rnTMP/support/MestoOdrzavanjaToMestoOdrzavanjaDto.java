package com.modul3.test.jwd48rnTMP.support;

import java.util.ArrayList;
import java.util.List;

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import com.modul3.test.jwd48rnTMP.model.MestoOdrzavanja;
import com.modul3.test.jwd48rnTMP.web.dto.MestoOdrzavanjaDTO;

@Component
public class MestoOdrzavanjaToMestoOdrzavanjaDto implements Converter<MestoOdrzavanja, MestoOdrzavanjaDTO> {

	@Override
	public MestoOdrzavanjaDTO convert(MestoOdrzavanja mestoOdrzavanja) {
		MestoOdrzavanjaDTO dto = new MestoOdrzavanjaDTO();
		dto.setId(mestoOdrzavanja.getId());
		dto.setDrzava(mestoOdrzavanja.getDrzava());
		dto.setGrad(mestoOdrzavanja.getGrad());
		dto.setPunoIme(mestoOdrzavanja.getGrad() + ", " + mestoOdrzavanja.getDrzava());
		return dto;
	}
	public List<MestoOdrzavanjaDTO> convert (List<MestoOdrzavanja> mestaOdrzavanja){
		List<MestoOdrzavanjaDTO> dtos = new ArrayList<>();
		for (MestoOdrzavanja mestoOdrzavanja : mestaOdrzavanja) {
			dtos.add(convert(mestoOdrzavanja));
		}
		return dtos;
	}
}
