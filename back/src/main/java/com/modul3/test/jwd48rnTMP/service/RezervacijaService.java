package com.modul3.test.jwd48rnTMP.service;

import java.util.List;

import com.modul3.test.jwd48rnTMP.model.Rezervacija;

public interface RezervacijaService {

	Rezervacija findOne(Long id);
	
	List<Rezervacija> findAll();
	
	Rezervacija save(Rezervacija rezervacija);	
}
