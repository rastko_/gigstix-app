package com.modul3.test.jwd48rnTMP.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.modul3.test.jwd48rnTMP.model.MestoOdrzavanja;

@Repository
public interface MestoOdrzavanjaRepository extends JpaRepository<MestoOdrzavanja, Long> {

	MestoOdrzavanja findOneById(Long id);
}
