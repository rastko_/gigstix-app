package com.modul3.test.jwd48rnTMP.service;

import java.util.List;

import org.springframework.data.domain.Page;

import com.modul3.test.jwd48rnTMP.model.Festival;

public interface FestivalService {

	Festival findOne(Long id);
	
	List<Festival> findAll();
	
	List<Festival> find(List<Long> ids);
	
	Page<Festival> findAll(int pageNo);
	
	Festival save(Festival festival);
	
	Festival update(Festival festival);
	
	Festival delete(Long id);

	Page<Festival> find(Long mestoOdrzavanjaId, String naziv, int pageNo);
	
//	Integer brojRadnikaPoOdeljenju(Long odeljenjeId);
}
