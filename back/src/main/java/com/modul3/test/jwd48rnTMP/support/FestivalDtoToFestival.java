package com.modul3.test.jwd48rnTMP.support;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import com.modul3.test.jwd48rnTMP.model.Festival;
import com.modul3.test.jwd48rnTMP.service.FestivalService;
import com.modul3.test.jwd48rnTMP.service.MestoOdrzavanjaService;
import com.modul3.test.jwd48rnTMP.web.dto.FestivalDTO;

@Component
public class FestivalDtoToFestival implements Converter<FestivalDTO, Festival> {

	@Autowired
	private FestivalService festivalService;
	
	@Autowired
	private MestoOdrzavanjaService mestoOdrzavanjaService;
	
	@Override
	public Festival convert(FestivalDTO dto) {
		Festival festival;
		if (dto.getId() != null) {
			festival = festivalService.findOne(dto.getId());
		} else {
			festival = new Festival();
		}
		if (festival != null) {
			festival.setBrojDostupnihKarata(dto.getBrojDostupnihKarata());
			festival.setCena(dto.getCena());
			if (dto.getMestoOdrzavanjaId() != null) {
				festival.setMestoOdrzavanja(mestoOdrzavanjaService.findOne(dto.getMestoOdrzavanjaId()));
			}
			festival.setNaziv(dto.getNaziv());
			festival.setPocetak(getLocalDate(dto.getPocetak()));
			festival.setZavrsetak(getLocalDate(dto.getZavrsetak()));
			
		}
		return festival;
	}
	
	private LocalDate getLocalDate(String date) throws DateTimeParseException {
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
		return LocalDate.parse(date, formatter);
	}

}
