package com.modul3.test.jwd48rnTMP.web.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.modul3.test.jwd48rnTMP.model.Festival;
import com.modul3.test.jwd48rnTMP.model.Rezervacija;
import com.modul3.test.jwd48rnTMP.service.FestivalService;
import com.modul3.test.jwd48rnTMP.service.RezervacijaService;
import com.modul3.test.jwd48rnTMP.support.RezervacijaDtoToRezervacija;
import com.modul3.test.jwd48rnTMP.support.RezervacijaToRezervacijaDto;
import com.modul3.test.jwd48rnTMP.web.dto.RezervacijaDTO;

@RestController
@RequestMapping(value = "/api/rezervacije", produces = MediaType.APPLICATION_JSON_VALUE)
@Validated
public class RezervacijaController {

	@Autowired
	private RezervacijaService rezervacijaService;
	
	@Autowired
	private FestivalService festivalService;
	
	@Autowired
	private RezervacijaToRezervacijaDto toRezervacijaDto;
	
	@Autowired
	private RezervacijaDtoToRezervacija toRezervacija;
	
	@PreAuthorize("hasRole('KORISNIK')")
	@GetMapping("/{id}")
	public ResponseEntity<RezervacijaDTO> getOne(@PathVariable Long id){
		Rezervacija rezervacija = rezervacijaService.findOne(id);
		if (rezervacija != null) {
			return new ResponseEntity<>(toRezervacijaDto.convert(rezervacija), HttpStatus.OK);
		} else {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
	}
	
	@PreAuthorize("hasAnyRole('KORISNIK', 'ADMIN')")
	@GetMapping
	public ResponseEntity<List<RezervacijaDTO>> getAll(){
		List<Rezervacija> rezervacije = rezervacijaService.findAll();
		return new ResponseEntity<>(toRezervacijaDto.convert(rezervacije), HttpStatus.OK);
	}
	
	@PreAuthorize("hasRole('KORISNIK')")
	@PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<RezervacijaDTO> create(@Valid @RequestBody RezervacijaDTO rezervacijaDTO){
		System.out.println();
		System.out.println(rezervacijaDTO);
		System.out.println();
		
		Rezervacija rezervacija = toRezervacija.convert(rezervacijaDTO);
		
		if (rezervacija.getFestival() == null || rezervacija.getFestival().getBrojDostupnihKarata() < rezervacija.getBrojKupljenihKarata()) {
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}
		
		Festival festival = rezervacija.getFestival();
		festival.setBrojDostupnihKarata(festival.getBrojDostupnihKarata() - rezervacija.getBrojKupljenihKarata());
		festivalService.save(festival);
		
		Rezervacija sacuvanaRezervacija = rezervacijaService.save(rezervacija);
		return new ResponseEntity<>(toRezervacijaDto.convert(sacuvanaRezervacija), HttpStatus.CREATED);
	}
}
