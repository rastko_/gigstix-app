package com.modul3.test.jwd48rnTMP.repository;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.modul3.test.jwd48rnTMP.model.Festival;

@Repository
public interface FestivalRepository extends JpaRepository<Festival, Long> {

	Festival findOneById(Long id);
	
	List<Festival> findByIdIn(List<Long> ids);
	
	@Query("SELECT f FROM Festival f WHERE " +
            "(:naziv = NULL OR f.naziv LIKE %:naziv%) AND " +
            "(:mestoOdrzavanjaId = NULL OR f.mestoOdrzavanja.id = :mestoOdrzavanjaId) " +
			"ORDER BY f.id")
	Page<Festival> search(@Param("mestoOdrzavanjaId")Long mestoOdrzavanjaId, @Param("naziv")String naziv, Pageable pageable);
	

}
