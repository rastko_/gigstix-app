package com.modul3.test.jwd48rnTMP.web.controller;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.modul3.test.jwd48rnTMP.model.Festival;
import com.modul3.test.jwd48rnTMP.service.FestivalService;
import com.modul3.test.jwd48rnTMP.service.MestoOdrzavanjaService;
import com.modul3.test.jwd48rnTMP.service.RezervacijaService;
import com.modul3.test.jwd48rnTMP.support.FestivalDtoToFestival;
import com.modul3.test.jwd48rnTMP.support.FestivalToFestivalDto;
import com.modul3.test.jwd48rnTMP.web.dto.FestivalDTO;

@RestController
@RequestMapping(value = "/api/festivali", produces = MediaType.APPLICATION_JSON_VALUE)
@Validated
public class FestivalController {

	@Autowired
	private FestivalService festivalService;
	
	@Autowired
	private MestoOdrzavanjaService mestoOdrzavanjaService;
	
	@Autowired
	private RezervacijaService rezervacijaService;
	
	@Autowired
	private FestivalToFestivalDto toFestivalDto;
	
	@Autowired
	private FestivalDtoToFestival toFestival;
	
	@PreAuthorize("hasAnyRole('KORISNIK', 'ADMIN')")
	@GetMapping("/{id}")
	public ResponseEntity<FestivalDTO> getOne(@PathVariable Long id){
		Festival festival = festivalService.findOne(id);
		if (festival != null) {
			return new ResponseEntity<>(toFestivalDto.convert(festival), HttpStatus.OK);
		} else {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
	}
	
	@PreAuthorize("hasAnyRole('KORISNIK', 'ADMIN')")
	@GetMapping
	public ResponseEntity<List<FestivalDTO>> getAll(
			@RequestParam(required = false) Long mestoOdrzavanjaId,
			@RequestParam(required = false) String naziv,
			@RequestParam(value="pageNo", defaultValue = "0") int pageNo){
		
		Page<Festival> pageFestival;
		
		if (naziv != null) {
			if (naziv.trim() == "") {
				naziv = null;
			}
		}
		if (mestoOdrzavanjaId != null) {
			if (mestoOdrzavanjaId == -1) {
				mestoOdrzavanjaId = null;
			}
		}
	
		
		pageFestival = festivalService.find(mestoOdrzavanjaId, naziv, pageNo);
		
//		pageFestival = festivalService.findAll(pageNo);
		HttpHeaders headers = new HttpHeaders();
		headers.add("total-pages", Integer.toString(pageFestival.getTotalPages()));
		
		
		return new ResponseEntity<>(toFestivalDto.convert(pageFestival.getContent()), headers, HttpStatus.OK);
		
	}
	
	@PreAuthorize("hasRole('ADMIN')")
	@PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<FestivalDTO> create(@Valid @RequestBody FestivalDTO festivalDTO){
		System.out.println();
		System.out.println(festivalDTO);
		System.out.println();
		Festival festival = toFestival.convert(festivalDTO);
		if (festival.getMestoOdrzavanja() == null) {
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}
		Festival sacuvanFestival = festivalService.save(festival);
		return new ResponseEntity<>(toFestivalDto.convert(sacuvanFestival), HttpStatus.CREATED);
	}
	
	@PreAuthorize("hasRole('ADMIN')")
	@PutMapping(value = "/{id}", consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<FestivalDTO> update(@PathVariable Long id, @Valid @RequestBody FestivalDTO festivalDTO){
		if (festivalDTO.getId() != id) {
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}
		
		Festival festival = toFestival.convert(festivalDTO);
		if (festival.getMestoOdrzavanja() == null) {
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}
		Festival updateFestival = festivalService.update(festival);
		return new ResponseEntity<>(toFestivalDto.convert(updateFestival), HttpStatus.OK);
	}
	
	@PreAuthorize("hasRole('ADMIN')")
	@DeleteMapping(value = "/{id}")
	public ResponseEntity<Void> delete(@PathVariable Long id){
		Festival festival = festivalService.delete(id);
		System.out.println();
		System.out.println(festival);
		System.out.println();
		if (festival == null) {
			return new ResponseEntity<Void>(HttpStatus.NOT_FOUND);
		} else {
			return new ResponseEntity<Void>(HttpStatus.NO_CONTENT);
		}
	}
	
	private LocalDate getLocalDate(String date) throws DateTimeParseException {
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
		return LocalDate.parse(date, formatter);
	}
}
